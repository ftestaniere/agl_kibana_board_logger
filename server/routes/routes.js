export default function (server, adminCluster, dataCluster) {

  server.route({
    path: '/api/board_logger/health',
    method: 'GET',
    async handler(req, h) {
      try {
          const response = await dataCluster.callWithRequest(req, 'cluster.health');
          return response;
      } catch (e) {
          console.error(e);
          return {"error": e};
      }
    }
  });

  //GET all the boards name
  server.route({
    path: '/api/board_logger/boards',
    method: 'GET',
    async handler(req, h) {
      try {
          const response = await dataCluster.callWithRequest(req, 'search', {
            index: 'boot-*',
            body: {
              "aggs" : {
                "patterns" : {
                  "terms" : { 
                    "field" : "board.keyword",
                    "size": 250
                  }
                }
              }
            }
          })
          return response
      } catch(e) {
          console.error(e)
          return {"error": e}
      }
    }
  })

    //GET all the commits
    server.route({
      path: '/api/board_logger/commits',
      method: 'GET',
      async handler(req, h) {
        try {
            const response = await dataCluster.callWithRequest(req, 'search', {
              index: 'log-*',
              body: {
                "aggs" : {
                  "patterns" : {
                    "terms" : { 
                      "field" : "git_commit.keyword",
                      "size": 250
                    }
                  }
                }
              }
            })
            return response
        } catch(e) {
            console.error(e)
            return {"error": e}
        }
      }
    })

  //GET logs from the selected board
  server.route({
    path: '/api/board_logger/{board}/{gitCommit}/logs',
    method: 'GET',
    async handler(req, h) {
      try {
          const size = await dataCluster.callWithRequest(req, 'count', {
            index: 'log-*',
            body: {
              "query": {
                "bool": {
                  "must": [
                    {
                      "query_string": {
                        "query": "board:" + req.params.board  
                      }
                    },
                    /*{
                      "term": {
                        "lvl": "target"
                      }
                    },*/
                    {
                      "term": {
                        "git_commit": req.params.gitCommit
                      }
                    }
                  ]
                }
              }
            }
          })

          let count
          if(size.count > 10000)
            count = 10000
          else
            count = size.count

          const response = await dataCluster.callWithRequest(req, 'search', {
            index: 'log-*',
            body: {
              "size": count,
              "query": {
                "bool": {
                  "must": [
                    {
                      "query_string": {
                        "query": "board:" + req.params.board 
                      }
                    },
                    /*{
                      "term": {
                        "lvl": "target"
                      }
                    },*/
                    {
                      "term": {
                        "git_commit": req.params.gitCommit
                      }
                    }
                  ]
                }
              }
            }
          })
          return response
      } catch(e) {
          console.error(e)
          return {"error": e}
      }
    }
  })

}
