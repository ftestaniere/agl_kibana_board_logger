import { resolve } from 'path';
import { existsSync } from 'fs';

import Route from './server/routes/routes';

export default function (kibana) {
  return new kibana.Plugin({
    require: ['elasticsearch'],
    name: 'board_logger',
    uiExports: {
      app: {
        title: 'Board Logger',
        description: 'read logs from your boards',
        main: 'plugins/board_logger/app',
        icon: 'inspect'
      },
      styleSheetPaths: [resolve(__dirname, 'public/app.scss')].find(p => existsSync(p)),
    },

    config(Joi) {
      return Joi.object({
        enabled: Joi.boolean().default(true),
      }).default();
    },

    init(server, options) { // eslint-disable-line no-unused-vars
      // Add server routes and initialize the plugin here
      const adminCluster = server.plugins.elasticsearch.getCluster('admin');
      const dataCluster = server.plugins.elasticsearch.getCluster('data');
      
      Route(server, adminCluster, dataCluster);
    }
  });
}
