import React from 'react';
import {
    EuiPage,
    EuiPageBody,
    EuiPageContent,
    EuiPageContentBody,
    EuiText,
    EuiComboBox,
    EuiPanel,
    EuiSpacer,
    EuiFlexGroup,
    EuiFlexItem,
    EuiButton,
    EuiCallOut,
    EuiFormRow,
} from '@elastic/eui';
import queryString from 'query-string'


export class Main extends React.Component {
    
    constructor(props) {
        super(props);

        this.logsFilterOptions = [
            {
                label: 'target',
                color: 'default'
            },
            {
                label: 'info',
                color: 'accent'
            },
            {
                label: 'warning',
                color: 'warning'
            },
            {
                label: 'error',
                color: 'danger'
            },
            {
                label: 'debug',
                color: 'secondary'
            }
        ]

        this.state = {
            boards: [],
            selectedBoards: [],
            commits: [],
            selectedCommits: [],
            selectedLogFilters: [], 
            bootResult: '',
            logs: [],
            displayedLogs: [{ msg:'No logs available' }],
            dataReady: false,
            loading: false
        };
    }

    /**
     * Events Handler methods
     */

    onBoardChange = selectedBoards => {
        this.setState({
            selectedBoards: selectedBoards,
        })
    }

    onCommitChange = selectedCommits => {
        this.setState({
            selectedCommits: selectedCommits,
        })
    }

    onLogFilterChange = selectedLogFilters => {
        this.setState({
            selectedLogFilters: selectedLogFilters,
            loading: true
        })

        if(selectedLogFilters.length > 0) {
            const filters = selectedLogFilters.map(filter => {
                return filter.label
            })
    
            const filteredLogs = this.state.logs.filter(log => (filters.indexOf(log.lvl) > -1))
            this.setState({ 
                displayedLogs: filteredLogs,
                loading: false
            })
        } else {
            this.setState({ 
                displayedLogs: this.state.logs,
                loading: false
            })
        }
     }

    onUpdateClick = () => {
        this.createLogs(this.state.selectedBoards[0].label, this.state.selectedCommits[0].label)
    }


    /**
     * Life cycle methods 
     */

    async componentDidMount() {
        await this.props.httpClient.get('../api/board_logger/boards').then((resp) => {
            const buckets = resp.data.aggregations.patterns.buckets
            let options = buckets.map((bucket) => {
                return { label: bucket.key }
            })
            this.setState({ boards: options })
        })

        await this.props.httpClient.get('../api/board_logger/commits').then((resp) => {
            const buckets = resp.data.aggregations.patterns.buckets
            let options = buckets.map((bucket) => {
                return { label: bucket.key }
            })
            this.setState({ commits: options })
        })

        if(location.search !== '') {
            const queries = queryString.parse(location.search)

            this.setState({ 
                selectedBoards: [{ label: queries.board }],
                selectedCommits: [{ label: queries.commit }]
            })

            this.createLogs(queries.board, queries.commit)
        }
    }


    /**
     * Private methods
     */

    createLogs = (board, commit) => {
        this.setState({ loading: true, dataReady: false })

        const filteredWords = ['failed', 'Kernel panic']
        this.props.httpClient.get('../api/board_logger/'+ board + '/' + commit + '/logs').then((resp) => {
            let msgs = resp.data.hits.hits.map((hit, index) => {
                if(index === resp.data.hits.hits.length - 1)
                    this.setState({ bootResult: hit._source.boot_result })
                return { msg: hit._source.msg, error: filteredWords.some(e => hit._source.msg.includes(e)), eno: index + 1, lvl: hit._source.lvl }
            })
            this.setState({ 
                logs: msgs,
                displayedLogs: msgs,
                dataReady: true,
                loading: false
            })
        })
    }


    render() {
        return (
            <EuiPage>
                <EuiPageBody>
                    <EuiPageContent>
                        <EuiPageContentBody>
                            <EuiPanel paddingSize="s">
                                <EuiFlexGroup>
                                    <EuiFlexItem> 
                                        <EuiComboBox
                                            placeholder="Select a board"
                                            singleSelection={{ asPlainText: true }}
                                            options={this.state.boards}
                                            selectedOptions={this.state.selectedBoards}
                                            onChange={this.onBoardChange}
                                            isClearable={false}
                                        />
                                    </EuiFlexItem>
                                    <EuiFlexItem>
                                        <EuiComboBox
                                            placeholder="Select a commit"
                                            singleSelection={{ asPlainText: true }}
                                            options={this.state.commits}
                                            selectedOptions={this.state.selectedCommits}
                                            onChange={this.onCommitChange}
                                            isClearable={false}
                                        />
                                    </EuiFlexItem>
                                    <EuiFlexItem grow={false}>
                                        <EuiButton
                                            iconType="arrowRight"
                                            iconSide="right"
                                            fill
                                            onClick={this.onUpdateClick}
                                            isLoading={this.state.loading}>
                                            Update
                                        </EuiButton>
                                    </EuiFlexItem>
                                </EuiFlexGroup>
                            </EuiPanel>
                            <EuiSpacer size="m" />
                            { this.state.dataReady && 
                                <EuiFormRow label="Log filters">
                                    <EuiComboBox
                                        placeholder="Select a log filter"
                                        options={this.logsFilterOptions}
                                        selectedOptions={this.state.selectedLogFilters}
                                        onChange={this.onLogFilterChange}
                                        isClearable={false}
                                    />
                                </EuiFormRow>
                            }
                                {
                                    this.state.dataReady && this.state.logs.length > 0 && (
                                        this.state.bootResult === 'pass' ? (
                                            <div>
                                                <EuiCallOut title="Boot pass" color="success" iconType="check"></EuiCallOut>
                                                <EuiSpacer size="s" />
                                            </div>
                                        ) : (
                                            <div>
                                                <EuiCallOut title="Boot fail" color="danger" iconType="cross"></EuiCallOut>
                                                <EuiSpacer size="s" />
                                            </div>
                                        )
                                    )
                                }
                            <EuiPanel paddingSize="s" style={{backgroundColor: 'black', height: '500px', overflowY: 'scroll'}}>
                                {
                                    this.state.displayedLogs.map((log, index) => {
                                        switch(log.lvl) {
                                            case 'info':
                                                return (<EuiText key={index} color="subdued"><p>{log.msg}</p></EuiText>)
                                            case 'debug':
                                                return (<EuiText key={index} color="secondary"><p>{log.msg}</p></EuiText>)
                                            case 'warning':
                                                return (<EuiText key={index} color="warning"><p>{log.msg}</p></EuiText>)
                                            case 'error':
                                                return (<EuiText key={index} color="danger"><p>{log.msg}</p></EuiText>)
                                            default:
                                                return (<EuiText key={index} color="ghost"><p>{log.msg}</p></EuiText>)
                                        }
                                    })
                                }
                            </EuiPanel>
                            <EuiSpacer />
                            <EuiText>Errors</EuiText>
                            <EuiPanel paddingSize="s" style={{ height: '200px', overflowY: 'scroll'}}>
                                {
                                    this.state.displayedLogs.map((log, index) => {
                                        if(log.error)
                                            return (<EuiText key={index}><p>line {log.eno} - {log.msg}</p></EuiText>)
                                    })
                                }
                            </EuiPanel>
                        </EuiPageContentBody>
                    </EuiPageContent>
                </EuiPageBody>
            </EuiPage>
        );
    }
}